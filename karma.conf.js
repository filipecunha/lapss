// Karma configuration
const webpackConfig = require('./webpack.config.dev');
const jqueryPath = 'http://app.globoesporte.globo.com/lib/jquery-1.8.3.min.js';
const analyticsPath = 'https://www.google-analytics.com/analytics.js';
const testFilesPath = './src/assets/scripts/**/*.test.js';
const sourceFilesRootPath = './src/assets/scripts/!(*.test|*.stub|*.min).js';
const sourceFilesPath = './src/assets/scripts/!(vendor)/**/*!(test|stub|min).js';

webpackConfig.entry = '';
webpackConfig.output = '';

module.exports = config => {
  config.set({
    files: [
      jqueryPath,
      analyticsPath,
      './src/assets/scripts/main.js',
      testFilesPath,
    ],

    preprocessors: {
      ['./src/assets/scripts/main.js']: ['webpack'],
      [testFilesPath]: ['webpack', 'babel']
    },

    webpack: webpackConfig,
    webpackMiddleware: {
      stats: 'errors-only'
    },

    frameworks: ['mocha', 'sinon-chai'],
    reporters: ['spec', 'coverage'],
    coverageReporter: {
      check: {
        each: {
          statements: 80,
          branches: 80,
          functions: 80,
          lines: 80,
        },
      },
      reporters: [
        { type: 'lcov', dir: 'coverage/', subdir: '.' },
        { type: 'json', dir: 'coverage/', subdir: '.' },
        {
          type: 'text',
        },
        {
          type: 'text-summary',
        },
      ],
    },
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: false,
    browsers: ['PhantomJS'],
    singleRun: true,
    concurrency: Infinity,
  });
};
