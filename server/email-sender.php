<?php
header("Access-Control-Allow-Origin: *");

if(isset($_POST['email'])) {
    $email_from = "contato@lapss.com.br";
    $email_to = "contato@ccqadv.com.br";
    $email_subject = "Contato Lapss";
}

// validação
if(!isset($_POST['name']) || !isset($_POST['email'])){
    die('Informações faltando');
}

$name = $_POST['name'];
$email = $_POST['email'];
$phone = $_POST['phone'];
$company = $_POST['company'];

$email_message = "Pedido de contato de:\n\n";

function clean_string($string) {
  $bad = array("content-type","bcc:","to:","cc:","href");
  return str_replace($bad,"",$string);
}

$email_message .= "Nome: ".clean_string($name)."\n";
$email_message .= "Email: ".clean_string($email_from)."\n";
$email_message .= "Telefone: ".clean_string($phone)."\n";
$email_message .= "Empresa: ".clean_string($company)."\n";

// headers
$headers = 'From: '.$email_from."\r\n".
'Reply-To: '.$email_from."\r\n" .
'X-Mailer: PHP/' . phpversion();
@mail($email_to, $email_subject, $email_message, $headers);
?>
