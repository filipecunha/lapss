import '../styles/main.scss';

const isProduction = location.hostname !== 'localhost' || location.hostname !== '127.0.0.1';

(($) => {
  $(document).ready(() => {
    if (isProduction) {
      return true;
    }

    return true;
  });
})(jQuery);

/* istanbul ignore if */
if (module.hot) {
  module.hot.accept();
}
