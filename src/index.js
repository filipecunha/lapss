$(document).ready(
    () => {
        // $('form').validate();
        $('form').validate(
            {
                debug: true,
                rules: {
                    name: {
                        required: true,
                        minlength: 3,
                    },
                    email: {
                        required: true,
                        email: true,
                    },
                },
                messages: {
                    //exemplo
                    name: {
                        required: "Digite seu nome",
                        minlength: "O nome deve conter ao menos 3 letras"
                    },
                    email: {
                        required: "Digite um email válido",
                    },
                },
                submitHandler: function(form) {
                   const name = $('#name').val();
                   const email = $('#email').val();
                   const phone = $('#phone').val();
                   const company = $('#company').val();

                    $.ajax({
                        type: "POST",
                        url: 'https://www.ccqadv.com.br/lapss/email-sender.php',
                        data: {name, email, phone, company},
                    })
                        .done(formSentSuccess)
                        .fail(formSentError)
                }
            }
        );

        formSentSuccess = () => {
            $('#name').val('');
            $('#email').val('');
            $('#phone').val('');
            $('#company').val('');

            $('.sent-success').show();
        };

        formSentError = () => {
            $('.sent-success').html('Erro ao enviar o contato.<br>Por favor envie email para<br>contato@lapss.com.br');
            $('.sent-success').show();
        };
    }
);
