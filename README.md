# Nome do projeto

Descrição do projeto

---

## Trabalhando com essa estrutura

`npm install` instala dependências

`npm start` inicia o projeto na porta 3000

`npm run server` utiliza o Browser Sync para rodar o projeto na porta 3000

`npm run lint` para reportar manualmente erros de lint no JS e SCSS

`npm run build`  para gerar a pasta ./dist para produção

`npm test` Roda o karma, e a bateria de testes criados com Mocha, Chai e Sinon

`npm run test:watch` Roda os testes em modo watch

`npm run share` Usa [ngrok](https://github.com/bubenshchykov/ngrok). Com o projeto rodando na porta 3000, execute o comando `npm run share` em outra aba no terminal. Um link para compartilhar o projeto será gerado.

---

## Incorporando componentes reutilizáveis

A equipe de desenvolvimento possui uma lista de componentes reutilizáveis em:

http://especial.globoesporte.globo.com/componentes/

`npm run list` lista todos os componentes disponíveis para baixar

`npm run get [COMPONENTE]` insere componentes no projeto

---

## Configurações do projeto

As configurações estão em `settings` no `package.json`

```
  "isNovoGCN": true,
  "pageTitle": "Título da Página",
  "description": "Descrição da página (compartilhamento)",
  "editorial": "editoria/de/publicacao",
  "articleName": "nome-da-materia"
```

* `isNovoGCN`: Define a versão do GCN que será publicado(utilizado nos arquivos de `webpack.dev` e `webpack.prod`)
  > `true`: novo  
  > `false`: velho

* `pageTitle`: Define o título da página que aparecerá na aba do navegador(tag `<title>`) e título de compartilhamento

* `description`: Define a descrição da página(tags de compartilhamento `<meta property=og:description>`) 

* `editorial`: Define a editoria de publicação da página
  > Usado para formar a url da página para as tags de compartilhamento
  >   
  > Formato (sem `/` no início e no fim): `editoria/de/publicacao`

* `àrticleName`: Define o nome da matéria
  > Usado para formar a url da página para as tags de compartilhamento
  >   
  > Formato (sem `/` no início e no fim): `nome-da-materia`
 
---

## Atualizando a página de produção no novo GCN
> __É necessário gerar o build (`npm run build`) do projeto, _commitar_ as mudanças,
incluindo a pasta dist (build), e realizar um _push_ na _branch master_ no
repositório do projeto no bitbucket.
Após mergeado/commitado na master, é preciso ir no [admin do novo GCN](http://gcn-dinamic-prod-blue.gcloud.globoi.com/admin),
selecionar o projeto e executar a ação 'atualizar página'.__
Dentro de poucos minutos a página já estará atualizada.

---

## Padrão de código
### Ferramentas
* [Husky](https://github.com/typicode/husky) cria githook e previne commits ruins
* [editorconfig](http://editorconfig.org/) é uma ferramenta para manter o estilo do código consistente entre diferentes editores e IDEs
* [stylelint](https://github.com/stylelint/stylelint) é um linter de SCSS
* [ESLint](http://eslint.org/) é um linter de JS

* [eslint-plugin-chai-friendly](https://www.npmjs.com/package/eslint-plugin-chai-friendly) torna a regra do eslint 'no-unused-expressions' amigável para o estilo de escrever o expect no chai


### Configurando as ferramentas dentro do editor de código
#### Editorconfig
* Sublime => Instala [um plugin](https://github.com/sindresorhus/editorconfig-sublime) para o editorconfig
* Webstorm => Instala, o plugin do editorconfig. Ir para `Preferences > Plugins` e buscar por editorconfig

#### ESlint
* Sublime => Instala [SublimeLinter](http://www.sublimelinter.com/en/latest/installation.html) e [SublimeLinter-eslint](https://github.com/roadhump/SublimeLinter-eslint)
* Webstorm => Habilite o ESLint em `Preferences > Language & Frameworks > Javascript > Code Quality Tools > ESLint`

#### Stylelint
* Primeiro intale [stylelint](https://github.com/stylelint/stylelint) globalmente `npm i stylelint -g`
* Sublime => Instalar [SublimeLinter](http://www.sublimelinter.com/en/latest/installation.html) e [SublimeLinter-contrib-stylelint](https://github.com/kungfusheep/SublimeLinter-contrib-stylelint)
* Webstorm => O Stylelint pode ser habilitado em `Preferences > Languages & Frameworks > Stylesheets > Stylelint`

### Regras para padronização de código
* [Airbnb JS Style Guide](https://github.com/airbnb/javascript) é um padrão de configuração do ESLint criado pela AirBnb
* [stylelint-config-standard](https://github.com/stylelint/stylelint-config-standard) Configuração padrão para o stylelint
* [JSDoc](http://usejsdoc.org/about-getting-started.html) é exigido em todas as funções

---

## Style
* [import-glob-loader](https://github.com/Aintaer/import-glob-loader), adiciona novo imports de SCSS automaticamente no main.scss. **Para cada novo arquivo SCSS criado no projeto, ainda é preciso reiniciar o servidor para visualizar as mudanças.**
* [bootstrap-grid](https://github.com/Hilzu/bootstrap-grid) Esse projeto somente injeta o grid do bootstrap no projeto, que pode ser útil para alinhamento de grid em um projeto.
* [sass-mq](https://github.com/sass-mq/sass-mq) é usado para criar breakpoints no SCSS

---

## Exibindo dados nos templates do pug

Para expor os dados de um JSON dentro dos arquivos pug, é preciso definir esses dados do JSON dentro do plugin HtmlWebpackPlugin, que fica configurado nos arquivos `webpack.config.dev.js` e `webpack.config.prod.js`

Exemplo:

```
const myData = require('./src/assets/data/data.json');
plugins: [
    new HtmlWebpackPlugin({
      extraJson,
      title: 'Index',
      favicon: './favicon.png',
      filename: 'index.html',
      template: './src/index.pug',
    })
]
```

Dentro do pug os dados estarão disponíveis em:

```
p= extraJson
```

---

## Teste e cobertura
### Ferramentas
* [Karma](https://github.com/karma-runner/karma) serve para rodar os testes com [PhantomJS launcher](https://github.com/karma-runner/karma-phantomjs-launcher)
* [Istanbul](https://github.com/gotwarlost/istanbul) para checar a cobertura de testes
* [Mocha](https://github.com/mochajs/mocha) é um framework de teste
* [Chai](https://github.com/chaijs/chai) para asserções dentro do Mocha
* [Sinon](https://github.com/sinonjs/sinon) para a criação de spies, stubs e mocks
* [sinon-chai](https://github.com/domenic/sinon-chai) para asserções customizadas do Chai, e melhor integração entre o Chai e o Sinon

### Rules
O limite mínimo de cobertura de código é 80% nos seguintes aspectos:

* Statements > 80%
* Branches > 80%
* Funções > 80%
* Linhas > 80%

Se a cobertura for menor que isso, o commit e o push serão rejeitados

---

## Convertendo dados que estão em uma planilha de Excel
Esse projeto usa [js-xls](https://github.com/SheetJS/js-xlsx) para converter Excel data em json

Com `npm run convert-data-to-json` é possível ver a conversão em ação

---

## Outras ferramentas
* [webpack 2](https://webpack.js.org/) como um module bundler para juntar os módulos de ES6
* [babel](http://babeljs.io/) para transpilar o novo JS para JS compatível com os browsers modernos
